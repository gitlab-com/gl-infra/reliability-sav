# Service Report
<!-- 
A service report should be used to provide relevant operational details to development teams who want or need to run their own service(s).  The report should provide an overview of the operational responsibilities associated with a service as well as the alerting profile so that folks have a better idea of what to expect. 
-->

## Summary
<!--
Use this section to give an overview of the service.  Include any highlights from the more detailed sections below
-->

## DRIs
<!--
There should be an individual engineer from both the Infrastructure team and the development team assigned as a DRI to all handovers.
-->
Infra DRI:
Service Owner Group DRI:
 
## Alerting Profile
### Alert Frequency
Number of Alerts last 30 days:
Number of Alerts last 180 days:
{+ Add Screenshot of search from Kibana along with link to search(es) +}

### Alert Requirement
<!--
Active alerting means the service needs a response to any alerts immediately.  Active alerts are handled by PagerDuty.
Passive alerting means the service does not require an immediate action.  Passive alerts tend to go to Slack channels or email as they are not immediately needed.  

For many services, passive alerting to a slack channel or email address is a better option than active alerting which requires many more resources.
This section should provide a recommended strategy for alerting.
-->
Alerting Requirement: active/passive (choose one)

## Service SLOs
<!--
Service SLOs are generated when a new service goes live.  Before a service handover it is useful to review the SLOs and ensure they still make sense.  This section should include information about current SLOs and what future SLOs should be along with a justification for any changes.
-->
Current SLO: (link to SLO)
New SLO:
If an SLO definition exists in [the service catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services), link that here along with proposed MR to change it as needed.
## Operational Details
### Runbooks
<!--
Link to any existing [runbooks](https://gitlab.com/gitlab-com/runbooks/-/tree/master) here
-->
### Readiness Review
<!--
[Readiness review](https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master) are typically done before a new service goes live.  The original readiness review should be linked here and reviewed.  In many cases it will make sense to do a new readiness review prior to service handover.  Use this section to record details around anything related to readiness reviews.
-->
### Access
<!--
Access changes within GCP will be required for any group that is planning to take over a service.  Note that even after handover is complete, Infrastructure should still retain access.
-->
GCP Project:
Access Level Required:


